<?php
/**
 * @package MFB Woocommerce Gifts
 * @version 1.0
 */
/*
Plugin Name:  MFB Woocommerce Gifts
Plugin URI: dergoldbroiler.de
Description: 
Author: Björn Zschernack
Version: 1.0
*/

// set inital session vars

add_action('init', 'set_line_step');
function set_line_step( $step ) {
	
if (WC()->session) {
    
     	if ( ! WC()->session->get( 'linestep' ) ) {
       WC()->session->set( 'linestep',0);        
     	}
}
}

/* 
* resaves cart session with new properties
*
*/
function update_variations() {    
    
    // only do something if a variation update was sent
    if ( isset( $_GET['variation'] ) ) {
     	
        //if the cart is empty do nothing
        if (WC()->cart->get_cart_contents_count() == 0) {
            return;
        }

        //array to collect cart items
        $cart_sort = [];

        //add cart item inside the array  wp_redirect(get_bloginfo('url').'/warenkorb/?variation_id='.$_POST['var_id'].'&variation='.$_POST['var_val'].'&pid='.$_POST['pid']);
        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {      
       
           $cart_sort[$cart_item_key] = WC()->cart->cart_contents[$cart_item_key];
           //be sure only to change the current chosen cart item    
           if (  $_GET['pid'] == $cart_sort[$cart_item_key]['product_id'] && $cart_sort[$cart_item_key]['is_gift'] == 1) {
		
		     $cart_sort[$cart_item_key]['data_hash'] = "";
             $cart_sort[$cart_item_key]['variation_id'] = $_GET['variation_id']; 
			 $cart_sort[$cart_item_key]['quantity'] = 1;
             $cart_sort[$cart_item_key]['variation'][ $_GET['attnames'] ] =  $_GET['variation'];
			 $cart_sort[$cart_item_key]['data']->set_price( 0 );
           } 
        }
		//  setSelectedOption('gruen');
	    
        //replace the cart contents with in the reverse order
        WC()->cart->cart_contents = $cart_sort;
       
    } else {
        return;
    }
}
add_action('woocommerce_cart_loaded_from_session', 'update_variations');

if ( ! function_exists( 'attribute_slug_to_title' ) ) {
	function attribute_slug_to_title( $attribute ,$slug ) {
		global $woocommerce;
		if ( taxonomy_exists( esc_attr( str_replace( 'attribute_', '', $attribute ) ) ) ) {
			$term = get_term_by( 'slug', $slug, esc_attr( str_replace( 'attribute_', '', $attribute ) ) );
			if ( ! is_wp_error( $term ) && $term->name )
				$value = $term->name;
		} else {
			$value = apply_filters( 'woocommerce_variation_option_name', $value );
		}
		return $value;
	}
}
/*
* ajax action to invoke the variations update method update_variations() 
* // call from js:callChange() in wcgifts_variations()
*/

function change_variation_in_cart() {	
    wp_redirect(get_bloginfo('url').'/warenkorb/?variation_id='.$_POST['var_id'].'&variation='.$_POST['var_val'].'&pid='.$_POST['pid'].'&attnames='.$_POST['attnames']); 
    exit;
}  
add_action( 'wp_ajax_change_variation_in_cart', 'change_variation_in_cart' );
add_action('wp_ajax_nopriv_change_variation_in_cart', 'change_variation_in_cart');


/*
* returns dropdown to change variations in cart
* including ajax call on the changeEvent
*/
function wcgifts_variations( $product_id, $citem ) {
    ?>
    <script>
    
    function callChange(selectVals) {
		
        var selectedID = selectVals.getAttribute('id');
      	//show message while changing variants
		jQuery.blockUI({ message: 'Die Varianten werden angepasst..',
		css: { width: '20%', padding:'30px', border:'0px solid #FFFFFF',cursor:'wait',backgroundColor:'#FFFFFF'},
		  overlayCSS:  { backgroundColor: '#FFFFFF',opacity:0.0,cursor:'wait'} 
		}); 

		var ajaxurl = jQuery('.var_select').attr('ajaxurl'); 
        var data = {
            'action': 'change_variation_in_cart',
            'pid': jQuery('#'+selectedID).attr('data-pid'),
            'attnames': jQuery('#'+selectedID).attr('data-att-name'),
            'var_id': jQuery('#'+selectedID+' option:selected').attr('data-option-id'),
            'var_val': jQuery('#'+selectedID+' option:selected').val(),
			'product_number': jQuery(this).attr('data-pnr')
        };

        jQuery.post(ajaxurl, data, function(response) {   
            jQuery('.woocommerce-cart-form').removeClass('processing');
           jQuery("[name='update_cart']").trigger("click"); 
            console.log("Resp:", response); jQuery.unblockUI();
       		
        });
     }
    </script>
    <?php  
    $variations_form = "";
    $cartitems = WC()->cart->get_cart();
    $available_variations = array();
    $mProduct = wc_get_product( $product_id );
    $mAttributes = $mProduct->get_attributes();
    $hasVariations = false;
      
      if ( count ( $cartitems[$citem]['variation']) > 0 &&  $cartitems[$citem]['is_gift'] == 1) { 
        $hasVariations = true;
        $mVariations = $mProduct->get_available_variations();
        $attributes = $mProduct->get_variation_attributes();
        $attribute_keys = array_keys( $attributes );
        
          foreach ( $attributes as $attribute_name => $options ) {
			 
			//set the gifts number to let wc know, which product to change on ajax call 
			if ( $product_id == get_option( 'pid1') ) {
				$product_number = 1;
			} else if ( $product_id == get_option( 'pid2') ) {
				$product_number = 2;
			} else {
				$product_number = 3;
			}
            $vars = get_variation_id_from_cart( $product_id );
            $product_variations = get_variations_by_productid ( $product_id, 'attribute_'.strtolower( $attribute_name ) );    
            $selected_att = $vars->var_arr['attribute_'.strtolower($attribute_name)];
            $selected_att_id =  $vars->var_id;
            $variations_form .= '<label for="'.sanitize_title( $attribute_name ).'">'.wc_attribute_label( $attribute_name ).'</label>';
            $variations_form .= '<form><select id="'.$attribute_name.'_'.$product_id.'" onchange="callChange(this)" class="var_select" id="'.sanitize_title( $attribute_name ).'" name="attribute_'.sanitize_title( $attribute_name ).'" data-current-att="'. $selected_att.'" ajaxurl="'.get_bloginfo('url').'/wp-admin/admin-ajax.php"; " data-current-att-id="'. $selected_att_id.'" data-att-name="attribute_'.strtolower( $attribute_name ).'" data-pid="'.$product_id.'"  data-pnr="'.$product_number.'">';
            $variations_form .= '<option value="ch">Wähle eine Option</option>';
                //  print_r ( $product_variations);
              foreach ( $product_variations as $option ) {
						 
				  if ( $product_number == 1 ) {
					  
					 if ( WC()->session->get( 'selected1' ) == $option->attribute_id || $_GET['variation_id'] == $option->attribute_id) {
						 $select = "selected";
						  WC()->session->set( 'selected1',  $option->attribute_id);
					 } else {
						 $select = ""; 
					 }
					  
					 $variations_form .= '<option  value="'.$option->attribute_value.'" data-option-id='. $option->attribute_id.' data-pid='. $option->product_id.' '.$select.'>'.attribute_slug_to_title($attribute_name,$option->attribute_value).'</option>';
                  } 
				  
				  else if ( $product_number == 2 ) { 
					  
					 if ( WC()->session->get( 'selected2' ) == $option->attribute_id || $_GET['variation_id'] == $option->attribute_id) {
						 $select2 = "selected";
						  WC()->session->set( 'selected2',  $option->attribute_id);
					 } else {
						 $select2 = ""; 
					 }
					  
					 $variations_form .= '<option  value="'.$option->attribute_value.'" data-option-id='. $option->attribute_id.' data-pid='. $option->product_id.' '.$select2.'>'.attribute_slug_to_title($attribute_name,$option->attribute_value).'</option>';
                  }
				  
				  else if ( $product_number == 3 ) {
					  
					 if ( WC()->session->get( 'selected3' ) == $option->attribute_id || $_GET['variation_id'] == $option->attribute_id) {
						 $select = "selected";
						  WC()->session->set( 'selected3',  $option->attribute_id);
					 } else {
						 $select = ""; 
					 }
					  
					 $variations_form .= '<option  value="'.$option->attribute_value.'" data-option-id='. $option->attribute_id.' data-pid='. $option->product_id.' '.$select3.'>'.attribute_slug_to_title($attribute_name,$option->attribute_value).'</option>';
                  }
				  
				  else {
					
					 $variations_form .= '<option  value="'.$option->attribute_value.'" data-option-id='. $option->attribute_id.' data-pid='. $option->product_id.'>'.$option->attribute_value.'</option>';
				  } 
			}
					                      
                  $variations_form .= '</select></form>';
              }     
        } 
       ?>

<?php
if ( is_cart() ) {	
    // check if product is a gift or not
    if ( in_array ( $product_id, WC()->session->get( 'gifts_array' ) ) ) {
         return $variations_form;
    } 
}
}

// define the woocommerce_cart_item_name callback 
function filter_woocommerce_cart_item_name( $product_get_name, $cart_item, $cart_item_key ) { 
   $cartitems = WC()->cart->get_cart();

	foreach($cartitems as $citem => $values) { 
   
        if ( $cart_item['is_gift'] == 1  ) {
               return $product_get_name.'<br><span class="gifts_title">Geschenk</span>';  //return $item_name;
        } else {
			  $product_get_name = $product_get_name;
		}
    }
	
    return $product_get_name; 
}; 
         
// add the filter 
add_filter( 'woocommerce_cart_item_name', 'filter_woocommerce_cart_item_name', 10, 3 ); 


function get_variation_id_from_cart( $product_id ) {
   if ( is_cart() ) {
  $cartitems = WC()->cart->get_cart();
  $varObj = new StdClass();
    foreach($cartitems as $cart_item) {
        $pp = wc_get_product( $cart_item['data']->get_parent_id() );
        if ( $cart_item['product_id'] == $product_id ) {
          $varObj->var_id = $cart_item['variation_id'];
          $varObj->var_arr = $cart_item['variation'];
         // return $cart_item['variation_id']
      }    
}
    return $varObj;
   }
}

add_filter('woocommerce_cart_item_name', 'add_variations_to_cart', 20, 3); 
function add_variations_to_cart($name, $cart_item, $cart_item_key) { 
   if ( !is_cart() ){
       return $name;
   }
    if ( is_cart() ) {
	$product_item = $cart_item['data'];
 
	// make sure to get parent product if variation
	if ( $product_item->is_type( 'variation' ) ) {
		$product_item = wc_get_product( $product_item->get_parent_id() );		
		echo '<div class="gifts_cart_title">'.$name.'</div>';
		$addedclass= "";
		if (empty(wcgifts_variations ( $product_item->get_id(),$cart_item_key) ) ) { 
			$addedclass = "empty";
		}
		print '<div class="gift_cart_variants '.$addedclass.'">'. wcgifts_variations ( $product_item->get_id(),$cart_item_key ) .'</div>';
	}  else {
		return $name;
	}
	 }
}


/*
* event listener for qty change
*
*/

add_action( 'wp_footer', 'prepare_ajax_functions' );
function prepare_ajax_functions() {

  if ( !is_admin() ) {
 
  // update cart via ajax and check for gifts in corresponding action	  
  ?>
<script>
		
	
   jQuery(document).on('click', '.qty', function (e) {
	 
     e.preventDefault();
     var qty = jQuery(this).val();
     var cart_item_key = jQuery(this).attr("id"); 

     jQuery.ajax({
		type: 'POST',
        dataType: 'json',
        url : "<?php echo get_bloginfo('url'); ?>/wp-admin/admin-ajax.php",
        data: {action : 'update_item_from_cart', 'cart_item_key' : cart_item_key, 'qty' : qty,  },
        success: function (data) {
         jQuery("[name='update_cart']").trigger("click"); 
     }

   });
}); 
</script>
<style>
	
	<?php
	  $cartitems = WC()->cart->get_cart();
	foreach( WC()->session->get( 'gifts_array' ) as $value ) {
	
			?>
	.class-<?php echo $value; ?> ul li.subscription-option{
		display:none;
	}
	<?php
			
	}
	  ?>
</style>

<?php 
  
  } 
}

/* logging, for debugging purpoeses only */
function logthis( $msg ) {
	?>
	<script>console.log('WC >> '.$msg);</script>
	<?php
	
	
}

function is_not_in_cart ( $product_id ) {
	$cartitems = WC()->cart->get_cart();
	foreach($cartitems as $cart_item_key => $value) {
		if ( $value['data']->get_id() == $product_id ) {
			return false;
			}
	}
	return true;
}

function update_item_from_cart() {
 return;
}
	
/*
* set gifts quantity to 1, always
*/
function set_uno () {
	$cartitems = WC()->cart->get_cart();
	foreach($cartitems as $cart_item_key => $value) {
		if (  $value['is_gift'] == 1) {
			 WC()->cart->set_quantity( $cart_item_key, 1);
			}
	}
}


/*
* manages adding and removing the gifts
*
*/
add_action( 'template_redirect', 'add_product_to_cart' );
function add_product_to_cart() {

  if ( ! is_admin() ) {
	 
	wcgifts_recalc_totals();
	  
	  $cartitems = WC()->cart->get_cart();
	foreach($cartitems as $cart_item_key => $value) {
		//echo '<br>Is Gift? => '.$value['is_gift'];
	}
	$total = WC()->session->get( 'tot');
    $wcgifts_options = wcgifts_get_options();	
	$cartitems = WC()->cart->get_cart();	
   
	$product = product_description();
    if ( $product[0]['price'] ) {

	// add gift 1  
    if ( $total >= $product[0]['price'] ){
		 
		  WC()->session->set( 'linestep',1 );   
		
		
	 if ( $product[0]['hasvar'] == 1 ) {	
		  WC()->cart->add_to_cart( $product[0]['product_id'], 1, $product[0]['variation_id'], $product[0]['variation'], array ('is_gift' => 1) );
		  WC()->session->set( 'linestep',1 );   
		  
		} else {
		  WC()->cart->add_to_cart( $product[0]['product_id'],1,0,array(),array ('is_gift' => 1)  );
		  WC()->session->set( 'linestep',1 );   
		 
		}
	}
	  
	// add gift 2
    if ( $total >= get_option('freeshippingprice') ){
		WC()->session->set( 'linestep',2 );   
	}

	// add gift 3  
    if ( $total >= $product[1]['price'] ){
		if ( $product[1]['hasvar'] == 1 ) {	 	
		  WC()->cart->add_to_cart( $product[1]['product_id'], 1, $product[1]['variation_id'], $product[1]['variation'], array ('is_gift' => 1)  );
		  WC()->session->set( 'linestep',3 );   
		 
		} else {
		  WC()->cart->add_to_cart( $product[1]['product_id'],1,0,array(),array ('is_gift' => 1)  );
		  WC()->session->set( 'linestep',3 );   
		 
		}
	}

	  
	// add gift 4  
    if ( $total >= $product[2]['price'] ){
		if ( $product[2]['hasvar'] == 1 ) {	 	
		  WC()->cart->add_to_cart( $product[2]['product_id'], 1, $product[2]['variation_id'], $product[2]['variation'], array ('is_gift' => 1)  );
		  WC()->session->set( 'linestep',4 );   
		 
		} else {
		  WC()->cart->add_to_cart( $product[2]['product_id'],1,0,array(),array ('is_gift' => 1) );
		  WC()->session->set( 'linestep',4 );   
		 
		}
	}  

	// remove gift 1  
	if ( $total < $product[0]['price'] ) {
		WC()->session->set( 'linestep',0 ); 
	    if ( $product[0]['hasvar'] == 1 ) {	 
	  	  remove_mycart_item( $product[0]['variation_ids'] );	
		  WC()->session->set( 'selected1', get_option('varid1'));   
	    } else {
		  remove_mycart_item( $product[0]['product_id'], true );	  
	    }
	}

	  
	// remove gift 2  
	if ( $total < $product[1]['price'] ) {
	  if ( $product[1]['hasvar'] == 1 ) {	 
	  	remove_mycart_item( $product[1]['variation_ids'] );	
		WC()->session->set( 'selected2', get_option('varid2'));   
	  } else {
		remove_mycart_item( $product[1]['product_id'], true );	  
	  }		
	} 

	  
	// remove gift 4  
	if ( $total <  $product[2]['price'] ) {
	  if ( $product[2]['hasvar'] == 1 ) {	 
	  	remove_mycart_item( $product[2]['variation_ids'] );
		WC()->session->set( 'selected3', get_option('varid3'));  
	  } else {
		remove_mycart_item( $product[2]['product_id'], true );	  
	  }
	}    
    }
	}
	set_uno();
}

function remove_mycart_item( $remove_me, $product=false ) {
	
  if ( $product == true ) {
	  foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
       if ( $cart_item['product_id'] == $remove_me && $cart_item['is_gift'] == 1) {
		   WC()->cart->remove_cart_item($cart_item_key);
	   }
      }
  }	else {
      foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
        if ( in_array ($cart_item['variation_id'],$remove_me)  && $cart_item['is_gift'] == 1) {
        //remove single product
        WC()->cart->remove_cart_item($cart_item_key);
       }
      }
  }
}




function get_product_variations( $product_id ) {
	global $wpdb;    
    $post_ids = array();
	
    $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}posts WHERE post_type='product_variation' and post_status = 'publish' and post_parent = ".$product_id, OBJECT );
    foreach ( $results as $result ) {
		array_push( $post_ids, $result->ID );
    }
	
	if ( count ( $post_ids ) > 0 ) {
		return $post_ids;
	} 
	
	return $post_ids;
}

add_action('wp_ajax_update_item_from_cart', 'update_item_from_cart');
add_action('wp_ajax_nopriv_update_item_from_cart', 'update_item_from_cart');

/** 
* invoked after cartchange (before calc totals)
* 
* sets all gift prices to zero
*/

function customize_gifts ( $cart_object ) {  
	
	$zero_products = array();
	$variations1 = array();
	$variations2 = array();
	$variations3 = array();
	//get variations
    if ( get_option('pid1') && get_option('pid1') != "") {
	   $variations1 = get_product_variations( get_option('pid1') );
       array_push ( $variations1, get_option('pid1') );
    }
    if ( get_option('pid2') && get_option('pid2') != "") {
	   $variations2 = get_product_variations( get_option('pid2') );
       array_push ( $variations2, get_option('pid2') );
    }
    if ( get_option('pid3') && get_option('pid3') != "") {
	    $variations3 = get_product_variations( get_option('pid3') );
        array_push ( $variations3, get_option('pid3') );
    }
	
	//put variations and product together
	
	
	
	
	//contains all products and variations that has to be zero, including changes in cart
	$zero_products = array_merge( $variations1, $variations2, $variations3 );
	WC()->session->set( 'gifts_array', $zero_products  );    
	
	$cartitems = WC()->cart->get_cart();
	
	//options
    $wcgifts_options = wcgifts_get_options();

        foreach (  $cartitems as $key => $value ) {
			
			if ( $value['is_gift'] == 1) {
				$value['data']->set_price(0);	
			}	
		}		
}
add_action( 'woocommerce_before_calculate_totals', 'customize_gifts', 99 );
//______________________________________________________________


// add scripts and styles
add_action('wp_enqueue_scripts', 'wcgifts_load_scripts');
function wcgifts_load_scripts($hook) {
  $my_css_ver = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'wcgifts.css' ));
  wp_register_style( 'my_css',    plugins_url( 'wcgifts.css',    __FILE__ ), false,   $my_css_ver );
  wp_enqueue_style ( 'my_css' );
    
    if ( is_cart() ) {
        //  WC()->cart->add_to_cart( 3007, 2, 70307, 'attribute_farbe', $cart_item_data );
        ?>
		<style>
		.sb-variation-select {
			max-width:50% !important;
		}
		</style>
		<?php
			}

	}
//______________________________________________________________


/*
* recalculate total price after adding products with certain prices as gifts to the cart (gifts with zero price)
* invoked on woocommerce_before_cart_totals-hook
*/
add_action( 'woocommerce_before_cart_totals', 'wcgifts_recalc_totals' );
add_action( 'woocommerce_add_to_cart', 'wcgifts_recalc_totals' );
function wcgifts_recalc_totals() {
    WC()->cart->calculate_totals(); 
	WC()->session->set( 'tot',floatval( preg_replace( '#[^\d.]#', '', WC()->cart->get_cart_total() )/100 ) );
}
add_action( 'woocommerce_calculate_totals', 'total_ok' );
function total_ok() {
	WC()->session->set( 'tot',floatval( preg_replace( '#[^\d.]#', '', WC()->cart->get_cart_total() )/100 ) );
}




    add_action( 'woocommerce_review_order_after_order_total', 'set_cart_gifts' );


//______________________________________________________________

function set_cart_gifts( $item, $type="" ) {
    $cartitems = WC()->cart->get_cart();
    foreach($cartitems as $citem => $values) { 
    
        if ( $values['data']->get_id() == $item->id ) {
            WC()->cart->remove_cart_item($citem); 
           break;
        }
    }
}
//______________________________________________________________

function get_line_width ( $step = 0 ) {
    $items = WC()->cart->get_cart();
    foreach($items as $citem => $values) { 
        $total = intval( ceil($values['line_subtotal']) + ceil($values['line_subtotal_tax']) );
        break;
    }
    //options
    $wcgifts_options = wcgifts_get_options();
    
    switch ( $step ) {
        case 0:
            return '10%';
            break;
        case 1:
            return '25%';
            break;
        case 2:
            return '47%';
            break;
        case 3:
            return '72%';
            break;
        case 4:
            return '100%';
            break;  
            
    }
}
function get_presents ( $step = 0 ) {
    switch ( $step ) {
        case 0:
            return '';
            break;
        case 1:
            return '.giftstep1';
            break;
        case 2:
            return '.giftstep1, .giftstep2';
            break;
        case 3:
            return '.giftstep1, .giftstep2, .giftstep3';
            break;
        case 4:
           return '.giftstep1, .giftstep2, .giftstep3, .giftstep4';
            break;  
            
    }
}
// set view
add_action( 'woocommerce_after_cart_contents', 'wcgifts_view' );

function wcgifts_view() {
    $wcgifts_options = wcgifts_get_options();
    $presents = get_presents( WC()->session->get( 'linestep' ) );
    $width = get_line_width( WC()->session->get( 'linestep' ) );  
?>

 <?php 
$titletext[0] = 'Nur ' . round ( ( get_option('price1') - WC()->session->get('tot')),2)  .' €  <span class="nomobile">bis zum ersten Geschenk</span>';
if ( WC()->session->get('linestep') >= 1 ) {
	$firstblock = "block";
	$secondblock = "";
	$thirdblock = "";
	$fourthblock = "";
	$titletext[0] = 'Nur ' . number_format(floatval( get_option('freeshippingprice') - WC()->session->get('tot') ), 2) .' € <span class="nomobile">bis zum nächsten Geschenk</span>';
	$firstgift = "style='color:#7c9f34;'";
} else {
	$firstgift = "";
}
	
if ( WC()->session->get('linestep') >= 2 ) {
	$titletext[0] = 'Nur ' . round( ( get_option('price2') - WC()->session->get('tot') ), 2) .' €  <span class="nomobile">bis zum nächsten Geschenk</span>';
	$firstblock = "block";
	$secondblock = "block";
	$thirdblock = "";
	$fourthblock = "";
	$secondgift = "style='color:#7c9f34;'";
} else {
	$secondgift = "";
}
	
	if ( WC()->session->get('linestep') >= 3 ) {
	$titletext[0] = 'Nur ' . number_format(floatval( get_option('price3')- WC()->session->get('tot') ), 2) .' €  <span class="nomobile">bis zum nächsten Geschenk</span>';	
	$firstblock = "block";
	$secondblock = "block";
	$thirdblock = "block";
	$fourthblock = "";
	$thirdgift = "style='color:#7c9f34;'";
} else {
	$thirdgift = "";
}
	
	if ( WC()->session->get('linestep') >= 4 ) {
	$titletext[0] = 'Alle Geschenke gesichert';			
	$firstblock = "block";
	$secondblock = "block";
	$thirdblock = "block";
	$fourthblock = "block";
	$fourthgift = "style='color:#7c9f34;'";
} else {
	$fourthgift = "";
}
	

?>
<table class="gifts-wrapper">
<tr><td>
<table class="gifts">
    <tr><td colspan="5" ><h3><?php echo esc_attr( get_option('plugintitlefrontend') ); ?></h3></td></tr>
    <tr>
		<td class="firsttd nomobile"><i class="fas fa-gift"></i></td>
        <td class="giftstep giftstep1" <?php echo $firstgift; ?>><?php if ( WC()->session->get('linestep') < 1 ) { echo "<span class='nogift nogift_one'>1. Geschenk</span>"; } else { echo "<span class='gift_reached gift_one_reached'>1. ".wc_get_product( get_option('pid1'))->get_title()."<span>"; } ?></td>        
		<td class="giftstep giftstep2" <?php echo $secondgift; ?>><?php if ( WC()->session->get('linestep') < 2 ) { echo "<span class='nogift nogift_two'>2. Geschenk</span>"; } else { echo "<span class='gift_reached gift_two_reached'>2. ".esc_attr( get_option('freeshippingtitle') )."<span>"; } ?></td>
        <td class="giftstep giftstep3" <?php echo $thirdgift; ?>><?php if ( WC()->session->get('linestep') < 3 ) { echo "<span class='nogift nogift_three'>3. Geschenk</span>"; } else { echo "<span class='gift_reached gift_three_reached'>3. ". wc_get_product( get_option('pid2'))->get_title()."<span>"; } ?></td> 
        <td class="giftstep giftstep4" <?php echo $fourthgift; ?>><?php if ( WC()->session->get('linestep') < 4 ) { echo "<span class='nogift nogift_four'>4. Geschenk</span>"; } else { echo "<span class='gift_reached gift_four_reached'>4.".wc_get_product( get_option('pid3'))->get_title()."<span>"; } ?></td>
    </tr>
	<tr class="gift-bar">
		<td class="p0 nomobile"><div class="singleline block"><div class="stext"><?php echo str_replace('.',',',$titletext[0]); ?></div></div></td>
        <td class="p0" ><div class="singleline <?php echo $firstblock; ?>">&nbsp;</div></td>
        <td class="p0"><div class="singleline <?php echo $secondblock; ?>">&nbsp;</div></td>
        <td class="p0"><div class="singleline <?php echo $thirdblock; ?>">&nbsp;</div></td>
        <td class="p0"><div class="singleline <?php echo $fourthblock; ?>">&nbsp;</div></td>
    </tr>
    <tr style="display:none;">
        <td colspan="5" class="gift1"><div class="line" style="width:<?php echo $width; ?>"><span class="title"><?php 
			if ( WC()->session->get('linestep') == 4 ) { echo 'Alle Geschenke gesichert.'; } else {
			echo 'Noch ' . intval( $wcgifts_options->pricetable[WC()->session->get('linestep')]- WC()->session->get('tot') ) .' € ...' ; } ?> </span></div></td>
    </tr>       
</table>
</td></tr>
</table>
<?php
  
}
//______________________________________________________________



//add_filter( 'woocommerce_package_rates', 'woocommerce_package_rates' );
/* function woocommerce_package_rates( $rates ) {
    print_r($rates);
    foreach($rates as $key => $rate ) {
        $rates[$key]->cost = $rates[$key]->cost - ( $rates[$key]->cost * ( $discount_amount/100 ) );
    }

    return $rates;
}
*/
function apply_static_rate($rates, $package)
    {                   
    
    foreach($rates as $key => $value) {
      //  print_r (  $rates);
            $rates[$key]->label = esc_attr( get_option('freeshippingtitle') );
            $rates[$key]->cost  =   0;    // your amount   
            $rates[$key]->taxes[6] = 0;
            

        }
        return $rates;
}



/**
 * get options
 */
function wcgifts_get_options(){
    $options = new StdClass();
    $pluginoptions = explode( ',', esc_attr( get_option('produktids') ) ); 
    $optionprices = explode( ',', esc_attr( get_option('produktprices') ) );
    $freeshippingprice = esc_attr( get_option('freeshippingprice') );
    $products = array( wc_get_product( $pluginoptions[0] ), wc_get_product( $pluginoptions[1] ), wc_get_product( $pluginoptions[2] ) );
    
    $options->pricetable = array($optionprices[0],$freeshippingprice,$optionprices[1],$optionprices[2]);
    $options->giftstable = array($pluginoptions[0],$pluginoptions[1],$pluginoptions[2]);
    $options->titletable = array( wc_get_product( $pluginoptions[0] ), wc_get_product( $pluginoptions[1] ), wc_get_product( $pluginoptions[2] ) );
    return $options;
}


add_filter('woocommerce_cart_item_price', 'set_old_price', 10, 3); 
function set_old_price($price, $cart_item, $cart_item_key) { 
	
    if ($cart_item[ 'data' ]->price == 0) { 
	  if ( $cart_item['variation_id'] ) {
		  $originalprice = wc_get_product( $cart_item[ 'variation_id' ] )->get_price();
	  } else {
		  $originalprice = wc_get_product( $cart_item[ 'product_id' ] )->get_price();
	  }
     
     $price = __( '<span class=redprice>'.$originalprice.' &euro;</span> - 0,00 &euro;', 'yourtheme'); 
    } 

    return $price; 
} 


function get_attribute_value ( $ID, $attribute ) {
    global $wpdb;    
    $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = '".$attribute."' and post_id = ".$ID, OBJECT );
   // echo "is_array ". count ( $results );
    if ( count ( $results ) > 0) {
       return $results[0]->meta_value;
    }
    return false;
}


function has_certain_attribute ( $ID, $attribute ) {
    global $wpdb;    
    $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = '".$attribute."' and post_id = ".$ID, OBJECT );
    if ( count ( $results ) > 0) {
        return true;
    }
    return false;
}

function get_variations_by_productid ( $product_id, $attribute ) {
    global $wpdb;    
    $attribute_options = array();
    $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}posts WHERE post_status = 'publish' and post_parent = ".$product_id, OBJECT );
    foreach ( $results as $result ) {
      $resultvars = new StdClass();       
      if ( has_certain_attribute ( $result->ID, $attribute ) ) {
          $resultvars->product_id = $product_id;
          $resultvars->attribute = $attribute;
          $resultvars->attribute_value = get_attribute_value ( $result->ID, $attribute );
          $resultvars->attribute_id = $result->ID;
          array_push ( $attribute_options, $resultvars );
      }
    }
    return $attribute_options;
}

function get_attribute_name ( $product_id ) {
    global $wpdb;    
    $attribute = array();
    $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key='_product_attributes' and post_id = ".$product_id, OBJECT );
		
    foreach ( $results as $result ) {
		
     $res = unserialize($result->meta_value);
	foreach ( $res as $re ) {
		if ( $re['is_variation'] == 1 ) {
			return $re['name'];
		}
			}
		//return $res[0];
      
    }
    return false;
}




/**
* returns products data
*
*/

function product_description () {
	
	/*
	* check if product data came from session (change in cart) 
	* or from options
	*
	*/

    $product = array();	
	// Define Product 1 (from options or session)  
	if (  WC()->session->get( 'pid1')  ){
		//WC()->session->set( 'selected1', $_GET['var_id']);  
		//WC()->session->set( 'selected1', WC()->session->get( 'vid1' ));  
		$product[0]['product_id'] =  WC()->session->get( 'pid1');
		$product[0]['hasvar'] = get_option('hasvar1');
		$product[0]['quantity'] = 1; 
		$product[0]['price'] = get_option('price1');
		
		if ( $product[0]['hasvar'] == 1 && get_option('pid1') ) {	    
	  		$product[0]['variation_id'] = WC()->session->get( 'vid1' );
			$product[0]['variation'] = array(
				esc_attr( get_option('prop1') ) => esc_attr( WC()->session->get( 'vval1' ) )
			);
			$product[0]['variations'] = wc_get_product( $product[0]['product_id'] )->get_available_variations(); 
			$product[0]['variation_ids'] = array();	
			foreach ( $product[0]['variations']  as $v ) {
					  array_push( $product[0]['variation_ids'], $v['variation_id']);
			}   
	  	}
	} else {
		if ( ! WC()->session->get( 'selected1' ) ) {
			WC()->session->set( 'selected1', get_option('varid1'));  
		}
		$product[0]['product_id'] =  get_option('pid1');
		$product[0]['hasvar'] = get_option('hasvar1');
		$product[0]['quantity'] = 1; 
		$product[0]['price'] = get_option('price1');
		
		if ( $product[0]['hasvar'] == 1 ) {	    
	  		$product[0]['variation_id'] = get_option('varid1');
			$product[0]['variation'] = array(
				esc_attr( get_option('prop1') ) => esc_attr( get_option('propval1') )
			);
			$product[0]['variations'] = wc_get_product( $product[0]['product_id'] )->get_available_variations(); 
			$product[0]['variation_ids'] = array();	
			foreach ( $product[0]['variations']  as $v ) {
					  array_push( $product[0]['variation_ids'], $v['variation_id']);
			}   
	  	}
	}
	
	
	// Define Product 2 (from options or session)  
	if (  WC()->session->get( 'pid2')  ){
			//WC()->session->set( 'selected2', $_GET['var_id']);  
		$product[1]['product_id'] =  WC()->session->get( 'pid2');
		$product[1]['hasvar'] = get_option('hasvar2');
		$product[1]['quantity'] = 1; 
		$product[1]['price'] = get_option('price2');
		
		if ( $product[1]['hasvar'] == 1 && get_option('pid1')) {	    
	  		$product[1]['variation_id'] = WC()->session->get( 'vid2' );
			$product[1]['variation'] = array(
				esc_attr( get_option('prop2') ) => esc_attr( WC()->session->get( 'vval2' ) )
			);
			$product[1]['variations'] = wc_get_product( $product[1]['product_id'] )->get_available_variations();
			$product[1]['variation_ids'] = array();	
			foreach ( $product[1]['variations']  as $v ) {
			  array_push( $product[1]['variation_ids'], $v['variation_id']);
			}
	  	}
	} else {
		if ( ! WC()->session->get( 'selected2' ) ) {
			WC()->session->set( 'selected2', get_option('varid2'));  
		}
		$product[1]['product_id'] =  get_option('pid2');
		$product[1]['hasvar'] = get_option('hasvar2');
		$product[1]['quantity'] = 1; 
		$product[1]['price'] = get_option('price2');
		
		if ( $product[1]['hasvar'] == 1 ) {	    
	  		$product[1]['variation_id'] = get_option('varid2');
			$product[1]['variation'] = array(
				esc_attr( get_option('prop2') ) => esc_attr( get_option('propval2') )
			);
			$product[1]['variations'] = wc_get_product( $product[1]['product_id'] )->get_available_variations();
			$product[1]['variation_ids'] = array();	
			foreach ( $product[1]['variations']  as $v ) {
			  array_push( $product[1]['variation_ids'], $v['variation_id']);
			}  
	  	}
	}
	 
	
	// Define Product 2 (from options or session)  
	if (  WC()->session->get( 'pid3')  ){
		$product[2]['product_id'] =  WC()->session->get( 'pid3');
		$product[2]['hasvar'] = get_option('hasvar3');
		$product[2]['quantity'] = 1; 
		$product[2]['price'] = get_option('price3');
		
		if ( $product[2]['hasvar'] == 1 && get_option('pid3')) {	    
	  		$product[2]['variation_id'] = WC()->session->get( 'vid3' );
			$product[2]['variation'] = array(
				esc_attr( get_option('prop3') ) => esc_attr( WC()->session->get( 'vval3' ) )
			);
			$product[2]['variations'] = wc_get_product( $product[2]['product_id'] )->get_available_variations(); 
			$product[2]['variation_ids'] = array();	
			foreach ( $product[2]['variations']  as $v ) {
					  array_push( $product[2]['variation_ids'], $v['variation_id']);
			}   
	  	}
	} else {
		if ( ! WC()->session->get( 'selected3' ) ) {
			WC()->session->set( 'selected3', get_option('varid3'));  
		} else {
			WC()->session->set( 'selected3', WC()->session->get( 'selected3' ) );
		}
		$product[2]['product_id'] =  get_option('pid3');
		$product[2]['hasvar'] = get_option('hasvar3');
		$product[2]['quantity'] = 1; 
		$product[2]['price'] = get_option('price3');
		
		if ( $product[2]['hasvar'] == 1 ) {	    
	  		$product[2]['variation_id'] = get_option('varid3');
			$product[2]['variation'] = array(
				esc_attr( get_option('prop3') ) => esc_attr( get_option('propval3') )
			);
			$product[2]['variations'] = wc_get_product( $product[2]['product_id'] )->get_available_variations(); 
			$product[2]['variation_ids'] = array();	
			foreach ( $product[2]['variations']  as $v ) {
					  array_push( $product[2]['variation_ids'], $v['variation_id']);
			}   
	  	}
	}	
	return $product;
}


function get_prop($pid) {
  $ps = wc_get_product( $pid )->get_attributes();

foreach ( $ps as $key => $value) {
	
		if ( $value['data']['variation'] == 1 ) {
			$prop = str_replace('pa_','',$value['data']['name']);
			echo "Als kleine Hilfe hier die Eigenschaft mit den Varianten (ID + Name der Variation):<br>";
			echo "Eigenschaft: <b>".ucfirst($prop).'</B>';
		}
	
}
	
}


/**
* options section
*/

// create custom plugin settings menu
add_action('admin_menu', 'wcgifts_menu');

function wcgifts_menu() {
 
	add_menu_page('Woocommerce Gifts', 'Woocommerce Gifts', 'administrator', __FILE__, 'wcgifts_plugin_settings_page' , plugins_url('/images/icon.png', __FILE__) );

	//call register settings function
	add_action( 'admin_init', 'wcgifts_plugin_settings' );
}


function wcgifts_plugin_settings() {
  
	//register our settings
    register_setting( 'wcgifts-plugin-settings-group', 'plugintitlefrontend' );
    register_setting( 'wcgifts-plugin-settings-group', 'freeshippingprice' );
    register_setting( 'wcgifts-plugin-settings-group', 'freeshippingtitle' );
	register_setting( 'wcgifts-plugin-settings-group', 'pid1' );
	register_setting( 'wcgifts-plugin-settings-group', 'pid2' );
	register_setting( 'wcgifts-plugin-settings-group', 'pid3' );
	register_setting( 'wcgifts-plugin-settings-group', 'price1' );
	register_setting( 'wcgifts-plugin-settings-group', 'price2' );
	register_setting( 'wcgifts-plugin-settings-group', 'price3' );
	register_setting( 'wcgifts-plugin-settings-group', 'hasvar1' );
	register_setting( 'wcgifts-plugin-settings-group', 'hasvar2' );
	register_setting( 'wcgifts-plugin-settings-group', 'hasvar3' );
	register_setting( 'wcgifts-plugin-settings-group', 'prop1' );
	register_setting( 'wcgifts-plugin-settings-group', 'propval1' );
	register_setting( 'wcgifts-plugin-settings-group', 'prop2' );
	register_setting( 'wcgifts-plugin-settings-group', 'propval2' );
	register_setting( 'wcgifts-plugin-settings-group', 'prop3' );
	register_setting( 'wcgifts-plugin-settings-group', 'propval3' );
	register_setting( 'wcgifts-plugin-settings-group', 'varid1' );
	register_setting( 'wcgifts-plugin-settings-group', 'varid2' );
	register_setting( 'wcgifts-plugin-settings-group', 'varid3' );
	register_setting( 'wcgifts-plugin-settings-group', 'subtext' );
    
    
}

function wcgifts_plugin_settings_page() {
?>
<style>
	div.props { display:none; }
	.container .col input[type="text"] {
		padding:10px;
		width:100%;
		max-width:250px;
		font-size:1.4em
	}
	.container .col {
		font-size:0.9em
	}
</style>
<script>
jQuery(document).ready(function() {
  if (jQuery('#hasvar1').prop('checked')) {
    jQuery('.props1').slideDown();
  } else {
    jQuery('.props1').slideUp();
  }
  jQuery('#hasvar1').change(function() {
    if (jQuery(this).prop('checked') == 1) {
      jQuery('.props1').slideDown();
    } else {
      jQuery('.props1').slideUp();
    }
  });
  if (jQuery('#hasvar2').prop('checked')) {
    jQuery('.props2').slideDown();
  } else {
    jQuery('.props2').slideUp();
  }
  jQuery('#hasvar2').change(function() {
    if (jQuery(this).prop('checked') == 1) {
      jQuery('.props2').slideDown();
    } else {
      jQuery('.props2').slideUp();
    }
  });
  if (jQuery('#hasvar3').prop('checked')) {
    jQuery('.props3').slideDown();
  } else {
    jQuery('.props3').slideUp();
  }
  jQuery('#hasvar3').change(function() {
    if (jQuery(this).prop('checked') == 1) {
      jQuery('.props3').slideDown();
    } else {
      jQuery('.props3').slideUp();
    }
  });
})
</script>
<div class="wrap">
<h1>'Woocommerce Gifts</h1>
<?php $url = plugins_url(); ?>
<?php
if ( get_option('pid1') ) {
    $p1 = wc_get_product( get_option('pid1'));
} else {
   $p1 = false;
}
if ( get_option('pid2') ) {
    $p2 = wc_get_product( get_option('pid2'));
} else {
   $p2 = false;
}
if ( get_option('pid3') ) {
    $p3 = wc_get_product( get_option('pid3'));
} else {
   $p3 = false;
}
 //   $product = wc_get_product( get_option('pid1'));
  
										 ?>
<form method="post" action="options.php">
    <?php settings_fields( 'wcgifts-plugin-settings-group' ); ?>
    <?php do_settings_sections( 'wcgifts-plugin-settings-group' ); ?>
	<p>
	<h3>Titel für Frontend</h3>
        <input type="text" style="width:350px;padding:20px;font-size:1.4em" name="plugintitlefrontend" value="<?php echo esc_attr( get_option('plugintitlefrontend') ); ?>" />
	</p>
	<p>
    <h3>Textbaustein für Spende</h3>
    <?php
         if ( get_option('subtext') ) {
             $subtext_value = esc_attr( get_option('subtext') );
         } else {
             $subtext_value = "Textbaustein";
         }                               
    ?>
	<input id="subtext" name="subtext" value="<?php echo $subtext_value; ?>">
	
	</p>
	<div class="container" style="display:flex;justify-content: space-between;max-width:80%">
		<div class="col">
			<h3>Geschenk 1</h3>
			<p><?php if ( $p1 != false ) { echo $p1->get_title(); } else { echo "Titel 1"; } ?></p>
			<b>ID des Produktes:</b><br>
			<input type="text" name="pid1" value="<?php echo esc_attr( get_option('pid1') ); ?>" /><br><br>
			<b>Preis, ab dem dieses Produkt in den Warenkorb gelegt wird:</b><br>
			<input type="text" name="price1" value="<?php echo esc_attr( get_option('price1') ); ?>" /><br><br>
			<b>Hat das Produkt Varianten?</b><br>
			<?php if ( esc_attr( get_option('hasvar1') ) == 1 ) {
				$selected = "checked";
			} else {
				$selected = "";
			}
			?>
			<input type="checkbox" <?php echo $selected; ?> name="hasvar1" id="hasvar1" value="1">: Ja<br><br>
			<div class="props1 props">
				<b>Wie heißt die Eigenschaft?:</b><br>
				<input type="text" name="prop1" value="<?php echo esc_attr( get_option('prop1') ); ?>" /><br><br>
				<b>Welchen Start/Standardwert soll die Eigenschaft haben?:</b><br>
				<input type="text" name="propval1" value="<?php echo esc_attr( get_option('propval1') ); ?>" /><br /><br />
				<b>Welchen ID hat die Variante?:</b><br>
				<input type="text" name="varid1" value="<?php echo esc_attr( get_option('varid1') ); ?>" /><br><br>
				<?php
					if ( $p1 != false ) {				 
                        get_prop( get_option('pid1') ); 
                        $vars1 = wc_get_product( get_option('pid1') )->get_available_variations();
                        foreach ( $vars1 as $var ) {
                            foreach ( $var['attributes'] as $attr ) {
                                echo '<br><b>'.$attr.'</b>';
                            }
                        }
                    }
					
			?>
			</div>
		</div>
		<div class="col">
			<h3>Geschenk 2</h3>
			<p><b>Kostenfreier Versand</b></p>
			<!-- <input type="text" style="width:250px"  name="freeshippingtitle" value="<?php echo esc_attr( get_option('freeshippingtitle') ); ?>" /> -->
			<b>Ist aktiv ab €:</b><br>
			<input type="text" name="freeshippingprice" value="<?php echo esc_attr( get_option('freeshippingprice') ); ?>" /> <br><br>
            <b>Anzuzeigender Titel:</b><br>
            <input type="text" style="width:250px"  name="freeshippingtitle" value="<?php echo esc_attr( get_option('freeshippingtitle') ); ?>">
            <br>
		</div>
		<div class="col">
			<h3>Geschenk 3</h3>
            <p><?php if ( $p2 != false ) { echo $p2->get_title(); } else { echo "Titel 2"; } ?></p>
			<b>ID des Produktes:</b><br>
			<input type="text" name="pid2" value="<?php echo esc_attr( get_option('pid2') ); ?>" /><br><br>
			<b>Preis, ab dem dieses Produkt in den Warenkorb gelegt wird:</b><br>
			<input type="text" name="price2" value="<?php echo esc_attr( get_option('price2') ); ?>" />
			<br><br>
			<b>Hat das Produkt Varianten?</b><br>
			<?php if ( esc_attr( get_option('hasvar2') ) == 1 ) {
				$selected = "checked";
			} else {
				$selected = ""; 
			}
			?>
			<input type="checkbox" <?php echo $selected; ?> name="hasvar2" id="hasvar2" value="1">: Ja
			<br><br>
			<div class="props2 props">
				<b>Wie heißt die Eigenschaft?:</b><br>
				<input type="text" name="prop2" value="<?php echo esc_attr( get_option('prop2') ); ?>" /><br><br>
				<b>Welchen Start/Standardwert soll die Eigenschaft haben?:</b><br>
				<input type="text" name="propval2" value="<?php echo esc_attr( get_option('propval2') ); ?>" /><br /><br />
				<b>Welchen ID hat die Variante?:</b><br>
				<input type="text" name="varid2" value="<?php echo esc_attr( get_option('varid2') ); ?>" /><br><br>
				<?php
				if ( $p2 != false ) {						 
					get_prop( get_option('pid2') ); 
					$vars1 = wc_get_product( get_option('pid2') )->get_available_variations();
					foreach ( $vars1 as $var ) {
						foreach ( $var['attributes'] as $attr ) {
							echo '<br><b>'.$attr.'</b>';
						}
					}
                }
					
			?>
			</div>
		</div>
		<div class="col">
			<h3>Geschenk 4</h3>
            <p><?php if ( $p3 != false ) { echo $p3->get_title(); } else { echo "Titel 3"; } ?></p>
			<b>ID des Produktes:</b><br>
			<input type="text" name="pid3" value="<?php echo esc_attr( get_option('pid3') ); ?>" /><br><br>
			<b>Preis, ab dem dieses Produkt in den Warenkorb gelegt wird:</b><br>
			<input type="text" name="price3" value="<?php echo esc_attr( get_option('price3') ); ?>" />
			<br><br>
			<b>Hat das Produkt Varianten?</b><br>
			<?php if ( esc_attr( get_option('hasvar3') ) == 1 ) {
				$selected = "checked";
			} else {
				$selected = "";
			}
			?>
			<input type="checkbox" <?php echo $selected; ?> name="hasvar3" id="hasvar3" value="1">: Ja<br><br>
			<div class="props3 props">
				<b>Wie heißt die Eigenschaft?:</b><br>
				<input type="text" name="prop3" value="<?php echo esc_attr( get_option('prop3') ); ?>" /><br><br>
				<b>Welchen Start/Standardwert soll die Eigenschaft haben?:</b><br>
				<input type="text" name="propval3" value="<?php echo esc_attr( get_option('propval3') ); ?>" /><br /><br />
				<b>Welchen ID hat die Variante?:</b><br>
				<input type="text" name="varid3" value="<?php echo esc_attr( get_option('varid3') ); ?>" /><br><br>
				<?php
				 if ( $p3 != false ) {					 
					get_prop( get_option('pid3') ); 
					$vars1 = wc_get_product( get_option('pid3') )->get_available_variations();
					foreach ( $vars1 as $var ) {
						foreach ( $var['attributes'] as $attr ) {
							echo '<br><b>'.$attr.'</b>';
						}
					}
                 }
			?>
			</div>
		</div>
		
	</div>
	<hr>
    <h3>Kleine Hilfe</h3>
	<p>Für alle Fälle finden Sie hier noch einmal alle Eigenschaften, die im System angelegt sind. Als Werte (Startwert, wie z.b. 600ml) immer die Werte aus der Spalte Titelform verwenden.</p>	
	<iframe width="100%" height="900" src="<?php echo get_bloginfo('url'); ?>/wp-admin/edit.php?post_type=product&page=product_attributes#attribute_orderby"></iframe>

    <?php submit_button(); ?>

</form>
</div>
<?php } 


add_action ( 'woocommerce_after_cart_totals', 'set_sentence');

function set_sentence() {
	?>
	<div class="submsg">
		<span class="submsgtxt">Mit deiner Bestellung gehen <?php echo   number_format( (WC()->session->get( 'tot' ) * 0.10 ), 2, ",", ".") .' &euro;';//esc_attr( get_option( 'subtext' ) ); ?> an die Organisation <?php echo esc_attr( get_option( 'woocommerce_demo_store_notice' ) ); ?>.</span>
	</div>
	<?php
}